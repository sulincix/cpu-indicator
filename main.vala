using Gtk;
using Posix;
using GLib;

public Gtk.Label turbo_label;
public Gtk.Menu menuSystem;
public bool i=true;

static void main(string[] args){
	Gtk.init(ref args);
	var trayicon = new StatusIcon.from_icon_name("gtk-home");
		trayicon.set_visible(true);
	trayicon.popup_menu.connect(menuSystem_popup);
	menuSystem = new Gtk.Menu();
	//turbo-boost
	if(isfile("/sys/devices/system/cpu/intel_pstate/no_turbo")){
		var turbo_box = new Box (Orientation.HORIZONTAL, 6);
		turbo_label = new Label ("Turbo Boost: ENABLED");
		var turbo_menuItem = new Gtk.MenuItem();
		turbo_box.add (turbo_label);
		turbo_menuItem.add (turbo_box);
		turbo_menuItem.activate.connect(turbo_event);
		menuSystem.append(turbo_menuItem);
	}
	//freq
	Gtk.Menu menufreq= new Gtk.Menu();
	var freq_menuItem = new Gtk.MenuItem();
	freq_menuItem.set_label("CPU");
	freq_menuItem.set_submenu(menufreq);
	int num=0;
	while(isfile("/sys/devices/system/cpu/cpu"+num.to_string())){
	    var freq_perf = new Gtk.MenuItem();
	        if(iscpuenable(num)){
	            freq_perf.set_label("Disable CPU"+num.to_string());
	        }else{
	            freq_perf.set_label("Enable CPU"+num.to_string());
	        }
	        int cpu=num;
	        freq_perf.activate.connect(()=>{
	            if(iscpuenable(cpu)){
	                Posix.system("pkexec su -c \"echo 0 > /sys/devices/system/cpu/cpu"+cpu.to_string()+"/online \"");
	            }else{
	                Posix.system("pkexec su -c \"echo 1 > /sys/devices/system/cpu/cpu"+cpu.to_string()+"/online \"");
	            }
	            if(iscpuenable(cpu)){
	                freq_perf.set_label("Disable CPU"+cpu.to_string());
	            }else{
	                freq_perf.set_label("Enable CPU"+cpu.to_string());
	            }
	        });
	        menufreq.append(freq_perf);
	    num++;
	}
	menuSystem.append(freq_menuItem);
	
	//governor
	Gtk.Menu menuGovernor= new Gtk.Menu();
	var governor_menuItem = new Gtk.MenuItem();
	governor_menuItem.set_label("Governor");
	governor_menuItem.set_submenu(menuGovernor);
	
	string[] modes=read_file("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors").split(" ");
	foreach(string perf in modes){
	    if(perf!=""){
	        var governor_perf = new Gtk.MenuItem();
	        governor_perf.set_label(perf);
	        menuGovernor.append(governor_perf);
	        governor_perf.activate.connect(()=>{
	            int i=0;
	            string cmd="pkexec su -c \"";
	            while(isfile("/sys/devices/system/cpu/cpu"+i.to_string())){
	                cmd=cmd+"echo "+perf+" > /sys/devices/system/cpu/cpu"+i.to_string()+"/cpufreq/scaling_governor ;";
	                i++;
	            }
	            cmd=cmd+"\"";
	            Posix.system(cmd);
	        });
	        
	    }
	}
	menuSystem.append(governor_menuItem);
	
	//backlight
	Gtk.Menu menuback= new Gtk.Menu();
	var back_menuItem = new Gtk.MenuItem();
	back_menuItem.set_label("Backlight");
	back_menuItem.set_submenu(menuback);
	GLib.Dir dir = GLib.Dir.open ("/sys/class/backlight/", 0);
	string dirname="";
	while ((dirname = dir.read_name ()) != null) {
	    string dname=dirname;
		var backx_menuItem = new Gtk.MenuItem();
		backx_menuItem.set_label(dname);
	    menuback.append(backx_menuItem);
	    backx_menuItem.activate.connect(()=>{
	        var w = new Gtk.Window();
	        w.set_title(dname);
	        var box=new Gtk.Box(Gtk.Orientation.HORIZONTAL,0);
	        var but=new Gtk.Button();
	        but.set_label("Set");
	        box.add(but);
	        float max=float.parse(read_file("/sys/class/backlight/"+dname+"/max_brightness"));
	        var s = new Gtk.Scale.with_range (Gtk.Orientation.HORIZONTAL, max/10, max, 1.0);
	        but.clicked.connect((range)=>{
	            Posix.system("pkexec su -c \"echo \""+s.get_value().to_string()+"\" > /sys/class/backlight/"+dname+"/brightness\"");
	        });
	        box.pack_end(s);
	        w.add(box);
	        box.set_size_request(400,50);
	        w.set_resizable(false);
	        w.show_all();
	    });
	}
	menuSystem.append(back_menuItem);
	
	//network
	Gtk.Menu menunet= new Gtk.Menu();
	var net_menuItem = new Gtk.MenuItem();
	net_menuItem.set_label("Network");
	net_menuItem.set_submenu(menunet);
	dir = GLib.Dir.open ("/sys/class/net/", 0);
	dirname="";
	while ((dirname = dir.read_name ()) != null) {
		string dname=dirname;
	    if(dname!="lo"){
	        var net_box = new Box (Orientation.HORIZONTAL, 6);
		    var net_label = new Label ("");
		    if(isnetenable(dname)){
    		    net_label.set_text(dname+": FORCE");
		    }else{
    		    net_label.set_text(dname+": AUTO");
		    }
		    var netx_menuItem = new Gtk.MenuItem();
		    net_box.add (net_label);
		    netx_menuItem.add (net_box);
	        menunet.append(netx_menuItem);
	        netx_menuItem.activate.connect(()=>{
	            if(isnetenable(dname)){
    		        net_label.set_text(dname+": AUTO");
    		        Posix.system("pkexec su -c \"echo auto > /sys/class/net/"+dname+"/device/power/control\"");
		        }else{
        		    net_label.set_text(dname+": FORCE");
    		        Posix.system("pkexec su -c \"echo on > /sys/class/net/"+dname+"/device/power/control\"");
		        }    
	        });
	    }
	}
	menuSystem.append(net_menuItem);
	
	//clear ram
	var ram_menuItem = new Gtk.MenuItem();
	ram_menuItem.set_label("Clear Ram");
	ram_menuItem.activate.connect(()=>{
	    Posix.system("pkexec su -c \"echo 3 > /proc/sys/vm/drop_caches\"");
	});
	menuSystem.append(ram_menuItem);
	
    //clear cache
	var cache_menuItem = new Gtk.MenuItem();
	cache_menuItem.set_label("Clear Cache");
	cache_menuItem.activate.connect(()=>{
	    Posix.system("rm -rf "+GLib.Environment.get_variable("HOME")+"/.cache &");
	    Posix.system("rm -rf /tmp/* &");
	});
	menuSystem.append(cache_menuItem);
	
	
	//sync
	var sync_menuItem = new Gtk.MenuItem();
	sync_menuItem.set_label("Sync");
	sync_menuItem.activate.connect(()=>{
	    Posix.system("sync");
	});
	menuSystem.append(sync_menuItem);

    //exit
	var exit_menuItem = new Gtk.MenuItem();
	exit_menuItem.set_label("Exit");
	exit_menuItem.activate.connect(Gtk.main_quit);
	menuSystem.append(exit_menuItem);
	
	menuSystem.show_all();
	menuGovernor.show_all();
	menufreq.show_all();
	Gtk.main();
}
private void menuSystem_popup(uint button, uint time) {
	  menuSystem.popup(null, null, null, button, time);
}
void turbo_event(){
	if(i){
		turbo_label.set_text("Turbo Boost: DISABLED");
		Posix.system("pkexec su -c \"echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo\"");
		Posix.system("pkexec su -c \"echo 0 > /sys/devices/system/cpu/cpufreq/boost\"");
	}else{
		turbo_label.set_text("Turbo Boost: ENABLED");
		Posix.system("pkexec su -c \"echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo\"");
		Posix.system("pkexec su -c \"echo 1 > /sys/devices/system/cpu/cpufreq/boost\"");
	}
	i=!i;
}
public bool isfile(string path){
	File file = File.new_for_path (path);
	return file.query_exists ();
}
public string read_file(string path){
    string data="";
	string tmp;
    if(isfile(path)){
	    File file = File.new_for_path (path);
	    try {
    		FileInputStream @is = file.read ();
		    DataInputStream dis = new DataInputStream (@is);
    
		    while ((tmp =dis.read_line ()) != null){
		    data=data+tmp;
		    }
	    } catch (Error e) {
	    }
	}
	return data;

}
public bool iscpuenable(int i){
    return read_file("/sys/devices/system/cpu/cpu"+i.to_string()+"/online")=="1" || ! isfile("/sys/devices/system/cpu/cpu"+i.to_string()+"/online");
}
public bool isnetenable(string dname){
    return read_file("/sys/class/net/"+dname+"/device/power/control")=="on";
}
